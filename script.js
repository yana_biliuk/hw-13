

document.addEventListener('DOMContentLoaded', function() {
    const tabs = document.querySelector('.tabs');
    const tabContents = document.querySelectorAll('.tab-content');

    tabs.addEventListener('click', function(event) {
      const targetTab = event.target.dataset.tab;
      if (!targetTab) return;

      tabContents.forEach(tabContent => {
        tabContent.classList.remove('active');
      });

      const contentShow = document.getElementById(targetTab);
      if (contentShow) {
        contentShow.classList.add('active');
      }
    });
  });